package com.vdopia.rpc.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * 
 * @author lokeshkohli
 *
 */
public class Configuration
{

	private static Config conf = ConfigFactory.load();
	
	public static Config getConfig() {
		return conf;
	}

	
}
