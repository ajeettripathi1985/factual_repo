package com.vdopia.rpc.factual;

import java.util.List;

public interface SegmentProvider {

	// This method is for Proximity Targeting
	public <T> List<MatchResult> queryMatch(Double latitude,Double longitude);

	// This method is for Audience Targeting
	public <T> List<MatchResult> queryAMatch(String userId);

}

