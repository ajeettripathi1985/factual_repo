package com.vdopia.rpc.factual;

import java.util.Map;

public interface MatchResult {
	
		public void setPayLoad(String payload);
		public String getPayLoad();
		public void setMetaData(Map<String,String> metaData);
		public Map<String,String> getMetaData();
		
		public void setSegmentId(String segmentId);
		public String getSegmentId();
		
		public void setSegmentName(String segmentName);
		public String getSegmentName();
		
		public void setSegmentPrice(int i);
		public int getSegmentPrice();
		
		public void setTargetCode(String targetingCode);
		public String getTargetCode();
		
		
}
