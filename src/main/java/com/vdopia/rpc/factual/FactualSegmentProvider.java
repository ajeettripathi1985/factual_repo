package com.vdopia.rpc.factual;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.factual.geopulse.Match;
import com.factual.geopulse.audience.AudienceSetClient;
import com.factual.geopulse.proximity.ProximityClient;
import com.factual.geopulse.proximity.ProximityClient.InvalidLocationException;

public class FactualSegmentProvider implements SegmentProvider {

	private final Logger logger = LoggerFactory.getLogger(FactualSegmentProvider.class);

	private AudienceSetClient audienceClient;
	private ProximityClient proximityClient;

	public FactualSegmentProvider() {

		audienceClient =
				AudienceSetClient.newClient(ProviderConstants.FACTUAL_ORGANIZATION_ID,
				ProviderConstants.VDOPIA_FACTUAL_APIKEY, ProviderConstants.VDOPIA_FACTUAL_SECRETKEY,
				ProviderConstants.FACTUAL_PATH, ProviderConstants.FACTUAL_MAXMEM);

		proximityClient = ProximityClient.newClient(ProviderConstants.FACTUAL_ORGANIZATION_ID,
				ProviderConstants.VDOPIA_FACTUAL_APIKEY, ProviderConstants.VDOPIA_FACTUAL_SECRETKEY,
				ProviderConstants.FACTUAL_PATH, ProviderConstants.FACTUAL_MAXMEM);

		try {
			this.loadStrategy();
		} catch (InterruptedException e) {
			logger.error("FactualSegmentProvider:InterruptedException:",e);
		}
	}

	/**
	 * Return list of Segments Names associated with Segments
	 * 
	 * @return
	 */
	public Set<String> getSegments() {
		return audienceClient.getActiveBuilds();
	}

	/**
	 * Returns list of Segment Ids
	 * 
	 * @return
	 */
	public Set<String> getSegmentIds() {
		Set<String> segments = audienceClient.getActiveBuilds();
		Set<String> segmentIdSet = new HashSet<String>();
		for (String seg : segments) {
			String segId = audienceClient.getDesignId(seg);
			segmentIdSet.add(segId);
		}

		return segmentIdSet;
	}

	public void defineStrategy() {

	}

	public void loadStrategy() throws InterruptedException {
		logger.info("loadStrategy started");
		
		proximityClient.awaitOK();
		audienceClient.awaitOK();
	}

	/**
	 * 
	 */
	public <T> List<MatchResult> queryMatch(Double latitude,Double longitude) {

		List<MatchResult> listResult = new ArrayList<MatchResult>();
		List<Match> listMatch = null;
		try {
			long requestPlacedTime = System.nanoTime();
			listMatch = proximityClient.testPoint(latitude, longitude, true);
			logger.debug("Proximity Client response time{}",System.nanoTime()-requestPlacedTime);
			
	 	} catch (IOException e) {
			//logger.error("IOException while doing querySegment:", e);
	 		logger.error("IOException while doing querySegment:",e);
		} catch (InvalidLocationException e) {
			//logger.error("InvalidLocationException while doing querySegment:", e);
			//System.out.println(e);
			logger.error("InvalidLocationException while doing querySegment {}" ,e);
		}

		if(listMatch!=null)
		{
		for (Match match : listMatch) {
			MatchResult mresult = new DefaultMatchResult();
			mresult.setPayLoad(match.getPayload());
			//mresult.setMetaData(mresult.getMetaData());
			mresult.setSegmentId(match.getDesignId());
			mresult.setSegmentName(match.getDesignName());
			mresult.setPayLoad(match.getPayload());
			mresult.setSegmentPrice(match.getPrice());
			mresult.setTargetCode(match.getTargetingCode());
			listResult.add(mresult);
		}
		}

		return listResult;
	}

	@Override
	public <T> List<MatchResult> queryAMatch(String userId) {
		List<MatchResult> listResult = new ArrayList<MatchResult>();
		List<Match> listMatch = null;
		try {
			long requestPlacedTime = System.nanoTime();
			listMatch = audienceClient.test(userId);
			logger.debug("Audience Client response time{}",System.nanoTime()-requestPlacedTime);
		} catch (IOException e) {
			logger.error("IOException while doing querySegment:", e);
		} 

		for (Match match : listMatch) {
			MatchResult mresult = new DefaultMatchResult();
			mresult.setPayLoad(match.getPayload());
			//mresult.setMetaData(mresult.getMetaData());
			mresult.setSegmentId(match.getDesignId());
			mresult.setSegmentName(match.getDesignName());
			mresult.setTargetCode(match.getTargetingCode());
//			System.out.println("Segment Id\t"+mresult.getSegmentId());
//    		System.out.println("Segment Name\t"+mresult.getSegmentName());
//    		System.out.println("Targeting Code\t"+mresult.getTargetCode());
			listResult.add(mresult);
		}

		return listResult;
	}

}
