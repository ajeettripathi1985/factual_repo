package com.vdopia.rpc.factual;

public class GeoPoint implements Targetable{
	
	private double latitude;
	private double longitude;
	private int weightage;
	
	public GeoPoint(double latitude,double longitude){
		this.longitude = longitude;
		this.latitude = latitude;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	/**
	 * @return the weightage
	 */
	public int getWeightage() {
		return weightage;
	}

	/**
	 * @param weightage the weightage to set
	 */
	public void setWeightage(int weightage) {
		this.weightage = weightage;
	}
	
}
