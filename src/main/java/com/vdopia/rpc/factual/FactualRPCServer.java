package com.vdopia.rpc.factual;

import com.vdopia.rpc.server.RPCServer;

public class FactualRPCServer {

	public static void main(String[] args) throws Exception {
		RPCServer rest = new RPCServer();
		rest.initialize();
		rest.start();
	}
}
