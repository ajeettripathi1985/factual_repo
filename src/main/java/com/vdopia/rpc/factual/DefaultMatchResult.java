package com.vdopia.rpc.factual;

import java.util.Map;

public class DefaultMatchResult implements MatchResult{

	private String payload;
	private String segmentId;
	private String segmentName;
	private int segmentPrice;
	private String targetCode;
	
	public void setPayLoad(String payload) {
		this.payload = payload;
	}

	public String getPayLoad() {
		// TODO Auto-generated method stub
		return payload;
	}

	public void setMetaData(Map<String, String> metaData) {
		// TODO Auto-generated method stub
		
	}

	public Map<String, String> getMetaData() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setSegmentId(String segmentId) {
		this.segmentId = segmentId;
	}

	public String getSegmentId() {
		return this.segmentId;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public String getSegmentName() {
		return this.segmentName;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public int getSegmentPrice() {
		return this.segmentPrice;
	}

	public void setSegmentPrice(int segmentPrice) {
		this.segmentPrice = segmentPrice;
	}

	
	public void setTargetCode(String targetCode) {
		this.targetCode = targetCode;
		
	}

	public String getTargetCode() {
		return this.targetCode;
	}

}
