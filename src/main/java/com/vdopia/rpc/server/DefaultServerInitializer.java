package com.vdopia.rpc.server;

import com.vdopia.rpc.protocol.RPCDecoder;
import com.vdopia.rpc.protocol.RPCEncoder;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

public class DefaultServerInitializer extends ChannelInitializer<SocketChannel> {


	public DefaultServerInitializer() {

	}

	@Override
	public void initChannel(SocketChannel ch) throws Exception {
		// Create a default pipeline implementation
		final ChannelPipeline p = ch.pipeline();

		p.addLast("decoder", new RPCDecoder(true));

		p.addLast("encoder", new RPCEncoder());

		p.addLast("handler", new DefaultHandler());
		
		p.addLast("httpExceptionHandler", new DefaultExceptionHandler());
	}
}
