package com.vdopia.rpc.server;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.rpc.util.Configuration;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.Slf4JLoggerFactory;

public class RPCServer implements Service {
	static {
		// initiate SLF4J Logger Factory setting
		InternalLoggerFactory.setDefaultFactory(new Slf4JLoggerFactory());
	}
	private final Logger logger = LoggerFactory.getLogger(RPCServer.class);
	private static HashMap<String, Object> objects = new HashMap<String, Object>();

	private int port = 9090;	
	private int backlog = 1024;
	private int numofworkerthreads = 20;

	public static Object getObject(String objName) {
		return objects.get(objName);
	}

	public RPCServer() throws Exception {

	}

	@Override
	public void initialize() {

		this.port = Configuration.getConfig().getInt("server.port");
		this.backlog = Configuration.getConfig().getInt("server.backlog");
		this.numofworkerthreads = Configuration.getConfig().getInt("server.numofworkerthreads");

		List<String> objClassList = Configuration.getConfig().getStringList("server.objects");
		logger.info("Object list:");
		try {

			for (String objClass : objClassList) {
				Object obj;
				logger.info("Loading objClass :"+objClass);
				obj = RPCServer.class.forName(objClass).newInstance();
				Class[] interfaces = obj.getClass().getInterfaces();
				for (int i = 0; i < interfaces.length; i++) {
					objects.put(interfaces[i].getName(), obj);
					logger.info("   " + interfaces[i].getName());
				}
			}

		} catch (InstantiationException e) {
			logger.error("InstantiationException:", e);
		} catch (IllegalAccessException e) {
			logger.error("IllegalAccessException:", e);
		} catch (ClassNotFoundException e) {
			logger.error("ClassNotFoundException:", e);
		}

	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	@Override
	public void start() {

		final EventLoopGroup bossGroup = new NioEventLoopGroup();
		final EventLoopGroup workerGroup = new NioEventLoopGroup(numofworkerthreads);
		try {

			int connectTimeout=10000;
			int writeBufferLowWaterMark = 32768;
			int writeBufferHighWaterMark = 65536;
			   
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
			.childHandler(new DefaultServerInitializer())
			.option(ChannelOption.SO_BACKLOG, this.backlog)
			.option(ChannelOption.SO_REUSEADDR, true)
			.option(ChannelOption.SO_KEEPALIVE, true)
			.option(ChannelOption.TCP_NODELAY, true)
			.childOption(ChannelOption.WRITE_BUFFER_LOW_WATER_MARK, writeBufferLowWaterMark)
			.childOption(ChannelOption.WRITE_BUFFER_HIGH_WATER_MARK, writeBufferHighWaterMark)
			.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectTimeout);
			Channel ch = b.bind(port).sync().channel();

			logger.info("NettyRPC server listening on port " + port + " and ready for connections...");

			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					bossGroup.shutdownGracefully();
					workerGroup.shutdownGracefully();
				}
			});
			ch.closeFuture().sync();

		} catch (InterruptedException e) {
			logger.error("InterruptedException:", e);
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}

	}

	public static void main(String[] args) throws Exception {
		RPCServer rest = new RPCServer();
		rest.initialize();
		rest.start();
	}
	
}
