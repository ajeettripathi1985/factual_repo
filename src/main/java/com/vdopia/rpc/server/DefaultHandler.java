package com.vdopia.rpc.server;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.rpc.protocol.Constants;
import com.vdopia.rpc.protocol.RPCContext;
import com.vdopia.rpc.protocol.Request;
import com.vdopia.rpc.protocol.Response;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class DefaultHandler extends SimpleChannelInboundHandler<RPCContext> {

	private final Logger logger = LoggerFactory.getLogger(DefaultHandler.class);
	
	public DefaultHandler() {
		super(false);
	}

	@Override
	protected void channelRead0(final ChannelHandlerContext ctx, final RPCContext rpcContext) throws Exception {
		processRequest(ctx,rpcContext);
	}	

	public void processRequest(ChannelHandlerContext ctx, RPCContext rpcContext){
//		System.out.println("Got a message in Default Handler");
		
		Request req = rpcContext.getRequest();
		Response res= new Response();
//		System.out.println("Seq Number\t"+req.getSeqNum()+ "Version is\t"+req.getVersion()+
//				"Request Type\t"+req.getType()+ "Serializer\t" +req.getSerializer()+
//				"Object Name\t"+req.getObjName()+"Function Name\t"+req.getFuncName());
//		//copy properties
		res.setSeqNum(req.getSeqNum());
		res.setVersion(req.getVersion());
		res.setType(req.getType());
		res.setSerializer(req.getSerializer());
		res.setObjName(req.getObjName());
		res.setFuncName(req.getFuncName());

		try{
		    
			Object[] args = req.getArgs();
			Class[] argTypes = new Class[args.length];
			String methodKey ="";
			for(int i=0;i<args.length;i++){
				argTypes[i] = args[i].getClass();
				methodKey+=argTypes[i].getSimpleName();
			}
	
			Object obj= RPCServer.getObject(req.getObjName());
			Class clazz= obj.getClass();
			Method func = clazz.getMethod(req.getFuncName(), argTypes);
			Object result= func.invoke(obj, req.getArgs());
			
			if(req.getType() != Constants.RPCType.oneway){
				res.setResult(result);
				res.setStatus(Constants.RPCStatus.ok);
				res.setMsg("ok");

				rpcContext.setResponse(res);
				ctx.writeAndFlush(rpcContext);
			}
			
		} catch (Exception e) {
			
			//pass exception message to client
			if(req.getType() != Constants.RPCType.oneway){
				res.setStatus(Constants.RPCStatus.exception);
				res.setMsg("excepton="+e.getClass().getSimpleName()+"|msg="+e.getMessage());
				
				rpcContext.setResponse(res);
				ctx.writeAndFlush(rpcContext);
			}
			
			//logger.error("DefaultHandler|processRequest got error",e);
			logger.error("DefaultHandler|processRequest got error");
		}
		
	}
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		super.channelInactive(ctx);
		// TODO(adolgarev): cancel submitted tasks,
		// that works only for not in progress tasks
		// if (future != null && !future.isDone()) {
		// future.cancel(true);
		// }
	}

}
