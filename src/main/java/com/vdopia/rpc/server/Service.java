package com.vdopia.rpc.server;

public interface Service {

		public void initialize();
		public void shutdown();
		public void start();
		
}
