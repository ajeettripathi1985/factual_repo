package com.vdopia.factual.rpc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.factual.geopulse.Match;
import com.factual.geopulse.audience.AudienceSetClient;
import com.factual.geopulse.proximity.ProximityClient;

/**
 * Hello world!
 *
 */
public class TestClient
{
	private static final String MY_KEY = "ofRqGvyrxiV94NruIV899Y2YQXTK8NBs8Ul8YJxm";
    private static final String MY_SECRET = "v1W8p9Xy8zN8BKcFdm68UNk1HPmnF2e4tQbf4cEn";

    private static final String Organization_id = "86e24a55-5f56-4b39-ae5a-cc49e3fd24bb";

    public static void main( String[] args )
   {
        
        System.out.println("Starting client");
        
       ProximityClient proximityClient =  ProximityClient.newClient(Organization_id, MY_KEY, MY_SECRET, "/tmp/test", 1024*1024*500);
       
       try {
           System.out.println("Await for rules");
           proximityClient.awaitOK();

           System.out.println("Start matching");
           
           long startTime = System.nanoTime();
           for(int i =0;i<10;i++){
	           
        	   List<Match> matches = proximityClient.testPoint(27.699246, -97.350259);
        			   //testPoint(37.555889,-121.977927); // Burlingame, CA
	           
	           System.out.println("Total matches for Burlingame: "+matches.size());
	           
	           for (Match match: matches) {
	               System.out.println("Match: "+match);
	               System.out.println("Match: "+match.getDesignName());
	               System.out.println("Match:getPayload: "+match.getPayload());
	           }
	           
	           matches = proximityClient.testPoint(37.555889,-121.977927); // Burlingame, CA
		       
		       System.out.println("Total matches for Fremont: "+matches.size());
		       
		       for (Match match: matches) {
		           System.out.println("Match: "+match);
		           System.out.println("Match: "+match.getDesignName());
		           System.out.println("Match:getPayload: "+match.getPayload());
		       }
           
           }
           
           long totaltime = System.nanoTime() - startTime;
           double average = totaltime/1000000;
           
           System.out.println("Total time taken :"+totaltime);
           System.out.println("average time taken :"+average);

           List<Match> matches = proximityClient.testPoint(36.7468, 119.7726); // Fresno, CA

           System.out.println("Total matches for Fresno: "+matches.size());

           for (Match match: matches) {
               System.out.println("Match: "+match);
           }

       } catch (Exception e) {
           System.out.println(e);
       }
   }
}